package me.gloomified.user.events;


import me.gloomified.user.smpessentials.SmpEssentials;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

public class Join implements Listener {
    @EventHandler
    public void above(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        PersistentDataContainer dataContainer = player.getPersistentDataContainer();
        if (!dataContainer.has(new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "Backpack"), PersistentDataType.STRING)) {
            dataContainer.set(new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "Backpack"), PersistentDataType.STRING, "");
        }
        for (int i = 0; i < SmpEssentials.players.size(); i++) {
            if(!player.isOp()){
                player.hidePlayer(SmpEssentials.getPlugin(SmpEssentials.class), SmpEssentials.players.get(i));
            }
        }
        if(SmpEssentials.get().getString(event.getPlayer().getUniqueId().toString()) == null){
            SmpEssentials.get().set(event.getPlayer().getUniqueId().toString() , 0);
            SmpEssentials.save();
        }

    }
}
