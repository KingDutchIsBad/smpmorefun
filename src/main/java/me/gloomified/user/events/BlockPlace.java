package me.gloomified.user.events;

import me.gloomified.user.api.Guardian;
import me.gloomified.user.sumo.ChatMsgUtil;
import me.gloomified.user.smpessentials.SmpEssentials;
import net.md_5.bungee.api.ChatColor;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Container;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Collection;
import java.util.UUID;

public class BlockPlace implements Listener {
    protected static UUID uuid;
    protected static UUID uuid2;
    protected  static UUID uuid3;
    protected static  UUID uuid4;
    protected static  UUID uuid5;
    private final int math = (int) (Math.random() * 5000);
    @EventHandler
    @SuppressWarnings("deprecated")
    public void above(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        if (player.getItemInHand().getItemMeta() == null) return;
        if (player.getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "Backpack")) {
            event.setCancelled(true);
        }
        if (player.getItemInHand().getItemMeta() == null) return;
        if (player.getItemInHand().getItemMeta().getLore() == null) return;
        if (player.getItemInHand().getType() == Material.AIR) return;
        if (player.getItemInHand().getItemMeta().getLore().contains(null)) return;
        if (player.getItemInHand().getItemMeta().getLore().contains(org.bukkit.ChatColor.DARK_GREEN + "Portable Crafting Table")) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void breakEvent(BlockBreakEvent event) {
        if (SmpEssentials.getPlugin(SmpEssentials.class).getConfig().getBoolean("Telepathy Shovel.playeruse")) {
            Player player = event.getPlayer();
            // deepcode ignore ApiMigration~getBlock: DOESN'T EXIST
            Block block = event.getBlock();
            if (player.getInventory().getItemInMainHand().getType() == Material.AIR) return;
            if (player.getInventory().getItemInMainHand().getItemMeta() == null) return;
            if (player.getInventory().getItemInMainHand().getItemMeta().getLore() == null) return;
            if (block.getState() instanceof Container) return;
            if (player.getInventory().getItemInMainHand().getItemMeta().getLore().contains(ChatColor.BLUE + "Telepathy Shovel")) {
                event.setDropItems(false);
                Collection<ItemStack> drops = block.getDrops(player.getInventory().getItemInMainHand());
                if (drops.isEmpty()) return;
                player.getInventory().addItem(drops.iterator().next());
            }
        }
    }

    @EventHandler
    public void forJobs(BlockBreakEvent event) {
        if (SmpEssentials.vault && SmpEssentials.getPlugin(SmpEssentials.class).getConfig().getBoolean("Jobs.enable")) {
            if (SmpEssentials.playersInJobs.contains(event.getPlayer())) {
                if (event.getBlock().getType().equals(Material.STONE) || event.getBlock().getType().equals(Material.ANDESITE) || event.getBlock().getType().equals(Material.DIORITE)
                        || event.getBlock().getType().equals(Material.DIAMOND_ORE) || event.getBlock().getType().isFuel() ||
                        // file deepcode ignore ApiMigration~getBlock: NONE
                        event.getBlock().getType().hasGravity() || event.getBlock().getType().equals(Material.EMERALD_ORE)) {
                    EconomyResponse response = SmpEssentials.getEconomy().depositPlayer(event.getPlayer(), 2.0);
                    if (response.transactionSuccess()) {
                        ChatMsgUtil.sendActionBar(event.getPlayer(), "&3You got &62$ &3for breaking " + event.getBlock().getType());
                    }
                }
            }
        }

    }

    @EventHandler
    public void forCustomMobs(BlockBreakEvent event) {
        if (event.getBlock().getType().equals(Material.STONE)) {
            Player player = event.getPlayer();
            if (math == 1221) {
                 LivingEntity entity = Guardian.getInstance().summonStoneGuardian(player.getWorld() , event.getBlock().getLocation() , new ItemStack(Material.LEATHER_HELMET) , null
                        , null , null ,  7 , 50 , 0.19 , "&6Stone Guardian" );
                uuid = entity.getUniqueId();
                ChatMsgUtil.sendMessage(player , "&4The Stone Guardian has been summoned try to kill it before it kills you.");
            }
        }
       if(event.getBlock().getType().equals(Material.DIAMOND_ORE)){
           if(math == 97){
               LivingEntity livingEntity = Guardian.getInstance().summonDiamondGuardian(event.getPlayer().getWorld(), event.getBlock().getLocation(), null,
                       null, null, null, 21, 150, 0.5, "&6Diamond Guardian");
               uuid2 = livingEntity.getUniqueId();
               ChatMsgUtil.sendMessage(event.getPlayer(), "&4The Diamond Guardian has been summoned, beware the best knights weren't able to kill");
               ChatMsgUtil.sendMessage(event.getPlayer(), "&6Illusioner's max health: &4"+ livingEntity.getHealth());
           }
       }
       if(event.getBlock().getType().equals(Material.COAL_ORE)){
           if(math == 950){
               LivingEntity entity = Guardian.getInstance().summonCoalGuardian(event.getPlayer().getWorld() , event.getBlock().getLocation() , null , new ItemStack(Material.LEATHER_CHESTPLATE)
                       , new ItemStack(Material.LEATHER_LEGGINGS) , null ,  8 , 50 , 0.15 , "&6Coal Guardian" );
               uuid3 = entity.getUniqueId();
           }
       }
    }
}
