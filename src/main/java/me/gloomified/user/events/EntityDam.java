package me.gloomified.user.events;

import me.gloomified.user.smpessentials.Bleed;
import me.gloomified.user.smpessentials.SmpEssentials;;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class EntityDam implements Listener {
    @EventHandler
    public void above(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player) {
            Player player = (Player) event.getEntity();
            if (player.getInventory().getChestplate() == null) return;
            else if(player.getInventory().getChestplate().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&',"&4Blood God Armour"))) {
              player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE , 300 , 2));
            }
            }
        }
    @EventHandler
    public void glowStickEffect(EntityDamageByEntityEvent event){
        if(event.getDamager() instanceof Player){
            Player player = (Player) event.getDamager();
            if(player.getInventory().getItemInMainHand().getItemMeta() == null) return;
            if(player.getInventory().getItemInMainHand().getItemMeta().getLore() == null) return;
            if(player.getInventory().getItemInMainHand().getItemMeta().getLore().contains(null)) return;
            if(player.getInventory().getItemInMainHand().getItemMeta().getLore().contains(ChatColor.GOLD + "Glow Effect I")){
                event.getEntity().setGlowing(true);
            }
        }
    }
    @EventHandler
    public  void  helmet(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player) {
            if (event.getDamager() instanceof Player) {
                Player player = (Player) event.getDamager();
                Player player1 = (Player) event.getEntity();
                if (player.getInventory().getHelmet() == null) return;
                if (player.getInventory().getHelmet().getItemMeta() == null) return;
                if (player.getInventory().getHelmet().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&4Blood God Armour"))) {
                    if (!player1.getActivePotionEffects().contains(new PotionEffect(PotionEffectType.POISON, 300, 2))) {
                        player1.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 300, 2));
                    }
                }

            }
        }
    }
    @EventHandler
    public void bleedStuff(EntityDamageByEntityEvent event){
        if(event.getEntity() instanceof LivingEntity){
            if(event.getDamager() instanceof Player){
                LivingEntity victim = (LivingEntity) event.getEntity();
                Player player = (Player) event.getDamager();
                if(player.getInventory().getItemInMainHand().getType() == Material.AIR) return;
                if(player.getInventory().getItemInMainHand().getItemMeta() == null) return;
                if(player.getInventory().getItemInMainHand().getItemMeta().getLore() == null) return;
                if(player.getInventory().getItemInMainHand().getItemMeta().getLore().contains(ChatColor.AQUA + "Ancient Sword")){
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c[Smp&4Essent&3ials] ")+ChatColor.DARK_RED + "Your Target is now bleeding.");
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c[Smp&4Essent&3ials] ")+ChatColor.DARK_RED + "Sometimes the target will have some health left and will not die.");
                   new Bleed(victim).runTaskTimer(SmpEssentials.getPlugin(SmpEssentials.class), 0L, 40L);
                }

            }
        }
    }
    @EventHandler
    public void healthStuff(EntityDamageByEntityEvent event){
        if(SmpEssentials.getPlugin(SmpEssentials.class).getConfig().getBoolean("Stick.playeruse")){
            if(event.getDamager() instanceof Player){
                Player player = (Player) event.getDamager();
                if(player.getInventory().getItemInMainHand().getType() == Material.AIR) return;
                if(player.getInventory().getItemInMainHand().getItemMeta() == null) return;
                if(player.getInventory().getItemInMainHand().getItemMeta().getLore() == null) return;
                if(player.getInventory().getItemInMainHand().getItemMeta().getLore().contains(null)) return;
                if(!(event.getEntity() instanceof  LivingEntity)) return;
                if(player.getInventory().getItemInMainHand().getItemMeta().getLore().contains(ChatColor.GOLD + "This stick shows the enemy's health")){
                    LivingEntity livingEntity = (LivingEntity) event.getEntity();
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c[Smp&4Essent&3ials] ") + ChatColor.DARK_AQUA
                            + "The damaged Entity's health is " +  livingEntity.getHealth());
                }
            }
        }
    }
    @EventHandler
    public void launchPadStuff(EntityDamageEvent event){
        if(event.getEntity() instanceof Player){
            if(event.getCause().equals(EntityDamageEvent.DamageCause.FALL) && SmpEssentials.playersThatJumped.contains((Player) event.getEntity())){
                SmpEssentials.playersThatJumped.remove((Player) event.getEntity());
                event.setCancelled(true);
            }
        }
    }
    @EventHandler
    public void guardianStuff(EntityDeathEvent event){
        if (event.getEntity().getType().equals(EntityType.ZOMBIE)) {
            if(event.getEntity().getEquipment().getHelmet().getType().equals(Material.LEATHER_HELMET) && event.getEntity().getCustomName().equalsIgnoreCase(ChatColor.GOLD+"Stone Guardian")
            && event.getEntity().getUniqueId().equals(BlockPlace.uuid)){
                event.getDrops().add(new ItemStack(Material.STONE , 100));
            }
        }
       if(event.getEntity().getType().equals(EntityType.ILLUSIONER)){
           if(event.getEntity().getUniqueId().equals(BlockPlace.uuid2)){
               event.getDrops().add(new ItemStack(Material.DIAMOND , 69));
           }
       }
       if(event.getEntity().getType().equals(EntityType.SKELETON)){
           if(event.getEntity().getUniqueId().equals(BlockPlace.uuid3)){
               event.getDrops().add(new ItemStack(Material.COAL , 89 ));
           }
       }
    }

}

