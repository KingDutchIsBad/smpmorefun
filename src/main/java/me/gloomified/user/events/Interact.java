package me.gloomified.user.events;

import me.gloomified.user.smpessentials.SmpEssentials;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

public class Interact implements Listener {

    @EventHandler
    public void above(PlayerInteractEvent event) {
        Player player = event.getPlayer();
       if(event.getItem() == null) return;
        if (!player.getInventory().getItemInMainHand().getItemMeta().hasDisplayName())  return;
                if (player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_RED + "God AXE")) {
                    @SuppressWarnings("unchecked")
                    Location location = player.getTargetBlock(null, 3000).getLocation();
                    if(location.getWorld() == null) return;
                    location.getWorld().strikeLightning(location);
                }

    }
    @EventHandler
    public  void onPlayerUseExplosionPick(PlayerInteractEvent event){
        Player player = event.getPlayer();
        if(event.getItem() == null) return;
        if(event.getClickedBlock() == null) return;
        if(player.getInventory().getItemInMainHand().getEnchantments().containsKey(Enchantment.getByKey(SmpEssentials.customEnchantments.getKey()))){
            Location location =  event.getClickedBlock().getLocation();
            if(location.getWorld() == null) return;
            location.getWorld().createExplosion( location , 4f);
        }
    }
    @EventHandler
    public  void backPackEvent(PlayerInteractEvent event){
        Player player = event.getPlayer();
        if(event.getItem() == null) return;
        if(player.getInventory().getItemInMainHand().getItemMeta() == null) return;
        if(player.getInventory().getItemInMainHand().getItemMeta().hasDisplayName()){
            if(player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD+"Backpack")){
                player.chat("/backpack");
            }
        }
    }
    @EventHandler
    public  void vanishStuff(PlayerInteractEvent event){
        Player player = event.getPlayer();
        if(event.getItem() == null) return;
        if(player.getInventory().getItemInMainHand().getType() == Material.AIR) return;
        if(player.getInventory().getItemInMainHand().getItemMeta() == null) return;
        if(player.getInventory().getItemInMainHand().getItemMeta().getLore() == null) return;
        if(player.getInventory().getItemInMainHand().getItemMeta().getLore().contains(null)) return;
        if(player.getInventory().getItemInMainHand().getItemMeta().hasDisplayName()){
            if(player.hasPermission("SmpEssentials.use.VanishHoe")){
                if(player.getInventory().getItemInMainHand().getItemMeta().getLore().contains(ChatColor.AQUA + "This hoe allows you to vanish")){
                    player.chat("/vanishtest");
                }
            }
        }
    }
    @EventHandler
    public void craftingTableStuff(PlayerInteractEvent event){
        if (SmpEssentials.getPlugin(SmpEssentials.class).getConfig().getBoolean("Crafting Table.playeruse")) {
            Player player = event.getPlayer();
            if(event.getItem() == null) return;
            if(player.getInventory().getItemInMainHand().getType() == Material.AIR) return;
            if(player.getInventory().getItemInMainHand().getItemMeta() == null) return;
            if(player.getInventory().getItemInMainHand().getItemMeta().getLore() == null) return;
            if(player.getInventory().getItemInMainHand().getItemMeta().getLore().contains(null)) return;
            if (player.getInventory().getItemInMainHand().getItemMeta().getLore().contains(ChatColor.DARK_GREEN + "Portable Crafting Table")){
                player.openWorkbench(null , true);
            }
        }
    }
    @EventHandler
    public void forLauncher(PlayerInteractEvent event){
        if(event.getItem() == null) return;
        if(event.getItem().getType() == Material.AIR) return;
        if(event.getItem().getItemMeta() == null) return;
        if(event.getItem().getItemMeta().getLore() == null) return;
        if(event.getItem().getItemMeta().getLore().contains(ChatColor.DARK_AQUA+"Launcher which launches you high up in the air")){
            event.getPlayer().setVelocity(new Vector(1.0 , 3.0 , 1.0));
            SmpEssentials.playersThatJumped.add(event.getPlayer());
        }
    }
}