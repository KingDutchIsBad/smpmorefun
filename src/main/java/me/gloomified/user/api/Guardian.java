package me.gloomified.user.api;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;

public class Guardian {
    private static final Guardian cool = new Guardian();

    public static Guardian getInstance(){
        return cool;
    }
    public  LivingEntity summonCoalGuardian (World world , Location location , ItemStack helmet , ItemStack chestplate , ItemStack
            leggings , ItemStack boots , double attackDamage , double health , double movementSpeed , String name){
        name = ChatColor.translateAlternateColorCodes('&' , name);
        LivingEntity coalGuardian = (LivingEntity) world.spawnEntity(location , EntityType.ZOMBIE);
        if(helmet != null) coalGuardian.getEquipment().setHelmet(helmet);
        if(chestplate != null) coalGuardian.getEquipment().setChestplate(chestplate);
        if(leggings != null) coalGuardian.getEquipment().setLeggings(leggings);
        if(boots != null) coalGuardian.getEquipment().setBoots(boots);
        coalGuardian.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).setBaseValue(attackDamage);
        coalGuardian.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(health);
        coalGuardian.setHealth(health);
        coalGuardian.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED).setBaseValue(movementSpeed);
        coalGuardian.setCustomName(name);
        return coalGuardian;
    }
    public  LivingEntity summonCoalGuardian(World world , Location location , ItemStack helmet , ItemStack chestplate , ItemStack
            leggings , ItemStack boots, String name ){
        name = ChatColor.translateAlternateColorCodes('&' , name);
        LivingEntity coalGuardian = (LivingEntity) world.spawnEntity(location , EntityType.ZOMBIE);
        if(helmet != null) coalGuardian.getEquipment().setHelmet(helmet);
        if(chestplate != null) coalGuardian.getEquipment().setChestplate(chestplate);
        if(leggings != null) coalGuardian.getEquipment().setLeggings(leggings);
        if(boots != null) coalGuardian.getEquipment().setBoots(boots);
        coalGuardian.setCustomName(name);
        return coalGuardian;
    }
    public  LivingEntity summonDiamondGuardian(World world , Location location , ItemStack helmet , ItemStack chestplate , ItemStack
            leggings , ItemStack boots, String name ){
        name = ChatColor.translateAlternateColorCodes('&' , name);
        LivingEntity diamondGuardian = (LivingEntity) world.spawnEntity(location , EntityType.ZOMBIE);
        if(helmet != null) diamondGuardian.getEquipment().setHelmet(helmet);
        if(chestplate != null) diamondGuardian.getEquipment().setChestplate(chestplate);
        if(leggings != null) diamondGuardian.getEquipment().setLeggings(leggings);
        if(boots != null) diamondGuardian.getEquipment().setBoots(boots);
        diamondGuardian.setCustomName(name);
        return diamondGuardian;
    }
    public  LivingEntity summonDiamondGuardian (World world , Location location , ItemStack helmet , ItemStack chestplate , ItemStack
            leggings , ItemStack boots , double attackDamage , double health , double movementSpeed , String name){
        name = ChatColor.translateAlternateColorCodes('&' , name);
        LivingEntity diamondGuardian = (LivingEntity) world.spawnEntity(location , EntityType.ZOMBIE);
        if(helmet != null) diamondGuardian.getEquipment().setHelmet(helmet);
        if(chestplate != null) diamondGuardian.getEquipment().setChestplate(chestplate);
        if(leggings != null) diamondGuardian.getEquipment().setLeggings(leggings);
        if(boots != null) diamondGuardian.getEquipment().setBoots(boots);
        diamondGuardian.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).setBaseValue(attackDamage);
        diamondGuardian.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(health);
        diamondGuardian.setHealth(health);
        diamondGuardian.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED).setBaseValue(movementSpeed);
        diamondGuardian.setCustomName(name);
        return diamondGuardian;
    }
    public  LivingEntity summonStoneGuardian(World world , Location location , ItemStack helmet , ItemStack chestplate , ItemStack
            leggings , ItemStack boots, String name ){
        name = ChatColor.translateAlternateColorCodes('&' , name);

        LivingEntity stoneGuardian = (LivingEntity) world.spawnEntity(location , EntityType.ZOMBIE);
        if(helmet != null) stoneGuardian.getEquipment().setHelmet(helmet);
        if(chestplate != null) stoneGuardian.getEquipment().setChestplate(chestplate);
        if(leggings != null) stoneGuardian.getEquipment().setLeggings(leggings);
        if(boots != null) stoneGuardian.getEquipment().setBoots(boots);
        stoneGuardian.setCustomName(name);
        return stoneGuardian;
    }
    public  LivingEntity summonStoneGuardian (World world , Location location , ItemStack helmet , ItemStack chestplate , ItemStack
            leggings , ItemStack boots , double attackDamage , double health , double movementSpeed , String name){
        name = ChatColor.translateAlternateColorCodes('&' , name);
        LivingEntity stoneGuardian = (LivingEntity) world.spawnEntity(location , EntityType.ZOMBIE);
        if(helmet != null) stoneGuardian.getEquipment().setHelmet(helmet);
        if(chestplate != null) stoneGuardian.getEquipment().setChestplate(chestplate);
        if(leggings != null) stoneGuardian.getEquipment().setLeggings(leggings);
        if(boots != null) stoneGuardian.getEquipment().setBoots(boots);
        stoneGuardian.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).setBaseValue(attackDamage);
        stoneGuardian.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(health);
        stoneGuardian.setHealth(health);
        stoneGuardian.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED).setBaseValue(movementSpeed);
        stoneGuardian.setCustomName(name);
        return stoneGuardian;
    }
}
