package me.gloomified.user.sumo;

import java.util.*;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import me.gloomified.user.smpessentials.SmpEssentials;

public class Events implements Listener {
    @EventHandler
    public void onWalkEvent(PlayerMoveEvent e){
        Location loc = e.getPlayer().getLocation();
        // deepcode ignore ApiMigration~getBlock: <The reason is because that thing isn't a thing in this api>
        Block locBlock = loc.getBlock();
        Material block = locBlock.getType();

        if (block == Material.WATER && ArenaManager.getManager().isInGame(e.getPlayer())) {
            Arena a = ArenaManager.getManager().getArenaFromPlayer(e.getPlayer());
            ArenaManager.getManager().removePlayer(e.getPlayer());
            ChatMsgUtil.sendTitle(e.getPlayer() , "&3You lost :(" , "&3Try Again next time!");
            try {
                Iterator<String> iterator = a.getPlayers().iterator();
                if (iterator.hasNext()){
                    String pnames = iterator.next();
                    UUID player = Bukkit.getPlayer(pnames).getUniqueId();
                    Player player1 = Bukkit.getPlayer(pnames);
                    ArenaManager.getManager().addWins(1 , player1);
                    if(SmpEssentials.vault){
                        SmpEssentials.getEconomy().depositPlayer(player1 , 50.0);
                    }
                    ChatMsgUtil.sendActionBar(player1, "&3YOU WON!");

                    ChatMsgUtil.sendTitle(player1, "&6Your Wins:", SmpEssentials.get().getString(player.toString()));
                    ArenaManager.getManager().removePlayer(player1);
                }
            }catch (ConcurrentModificationException ex){
               SmpEssentials.getPlugin(SmpEssentials.class).getLogger().severe(Arrays.toString(ex.getStackTrace()));
            }

        }
        if(ArenaManager.getManager().isInGame(e.getPlayer())){
            Arena arena = ArenaManager.getManager().getArenaFromPlayer(e.getPlayer());
            if(arena.getPlayers().size() == 1){
                e.setCancelled(true);
                ChatMsgUtil.sendActionBar(e.getPlayer(), "&3Please wait while someone else joins");
            }

        }
    }
    @EventHandler
    public void onDeathEvent(PlayerDeathEvent event){
        if(ArenaManager.getManager().isInGame(event.getEntity().getPlayer())){
            ChatMsgUtil.sendActionBar(event.getEntity() , "&3You Lost :(");
            ChatMsgUtil.sendTitle(event.getEntity() , "&3You Lost" , "&3Try again next time!");
            ArenaManager.getManager().removePlayer(event.getEntity().getPlayer());
            Arena a = ArenaManager.getManager().getArenaFromPlayer(event.getEntity().getPlayer());
            try {
                Iterator<String> iterator = a.getPlayers().iterator();
                if (iterator.hasNext()){
                    String pnames = iterator.next();
                    UUID player = Bukkit.getPlayer(pnames).getUniqueId();
                    Player player1 = Bukkit.getPlayer(pnames);
                    ArenaManager.getManager().addWins(1 , player1);
                    if(SmpEssentials.vault){
                        SmpEssentials.getEconomy().depositPlayer(player1 , 50.0);
                    }
                    ChatMsgUtil.sendActionBar(player1, "&3YOU WON!");

                    ChatMsgUtil.sendTitle(player1, "&6Your Wins:", ArenaManager.getManager().getWins(player1));
                    ArenaManager.getManager().removePlayer(player1);
                }
            }catch (ConcurrentModificationException ex){
                SmpEssentials.getPlugin(SmpEssentials.class).getLogger().severe(Arrays.toString(ex.getStackTrace()));
            }
        }
    }
    @EventHandler
    public void onLeaveEvent(PlayerQuitEvent e){
        if(ArenaManager.getManager().isInGame(e.getPlayer())){
            ArenaManager.getManager().removePlayer(e.getPlayer());
        }
    }
}