package me.gloomified.user.stuff;

import me.gloomified.user.smpessentials.SmpEssentials;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class Launch {
    private final List<String> lore = new ArrayList<String>();

    public void recipe() {
        lore.add(ChatColor.BLUE + "----------");
        lore.add(ChatColor.DARK_AQUA + "Launcher which launches you high up in the air");
        lore.add(ChatColor.BLUE + "----------");
        ItemStack feather = new ItemStack(Material.FEATHER);
        ItemMeta itemMeta = feather.getItemMeta();
        itemMeta.setLore(lore);
        itemMeta.setDisplayName(ChatColor.DARK_AQUA + "Launcher");
        feather.setItemMeta(itemMeta);
        ShapedRecipe shapedRecipe = new ShapedRecipe(new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "LaunchPad_NamesKey"), feather);
        shapedRecipe.shape("BBB", "BFB", "BBB");
        shapedRecipe.setIngredient('B', Material.BLAZE_ROD);
        shapedRecipe.setIngredient('F', Material.FEATHER);
        if (SmpEssentials.getPlugin(SmpEssentials.class).getConfig().getBoolean("Launcher.enable")) {
            if(Bukkit.getServer().getRecipe(new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "LaunchPad_NamesKey")) == null){
                Bukkit.addRecipe(shapedRecipe);
            }
        }
    }
}
