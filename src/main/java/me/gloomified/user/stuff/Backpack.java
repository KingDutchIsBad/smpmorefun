package me.gloomified.user.stuff;

import me.gloomified.user.smpessentials.SmpEssentials;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class Backpack {
    SmpEssentials plugin = SmpEssentials.getPlugin(SmpEssentials.class);
    public void above(){
        ItemStack itemStack = new ItemStack(Material.WITHER_SKELETON_SKULL);
        ItemMeta itemMeta = itemStack.getItemMeta();
        if (itemMeta != null) itemMeta.setDisplayName(ChatColor.GOLD + "Backpack");
        ArrayList<String> lore = new ArrayList<>();
        lore.add(ChatColor.GOLD + "-----------------------");
        lore.add(ChatColor.GOLD + "Backpack");
        lore.add(ChatColor.GOLD + "-----------------------");
        if (itemMeta != null) itemMeta.setLore(lore);
        itemStack.setItemMeta(itemMeta);
         NamespacedKey namespacedKey = new NamespacedKey(plugin, "Backpack_key");
        ShapedRecipe shapedRecipe = new ShapedRecipe(namespacedKey , itemStack);
        shapedRecipe.shape("GGG" , "GHG" , "GGG");
        shapedRecipe.setIngredient('G' , Material.GOLD_INGOT);
        shapedRecipe.setIngredient('H' , Material.WITHER_SKELETON_SKULL);
        if(plugin.getConfig().getBoolean("Backpack.craft")  && Bukkit.getServer().getRecipe(namespacedKey) == null){
            plugin.getServer().addRecipe(shapedRecipe);
        }

    }
}
