package me.gloomified.user.stuff;

import me.gloomified.user.smpessentials.SmpEssentials;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class teleport_bow {
    SmpEssentials plugin = SmpEssentials.getPlugin(SmpEssentials.class);
    public void bow(){
        ArrayList<String> lore = new ArrayList<>();
        lore.add(ChatColor.BLUE + "---------------");
        lore.add(ChatColor.BLUE+ "Teleport Bow");
        lore.add(ChatColor.BLUE + "---------------");
        ItemStack itemStack = new ItemStack(Material.BOW);
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setLore(lore);
        itemMeta.setDisplayName(ChatColor.DARK_BLUE + "Teleport Bow");
        itemStack.setItemMeta(itemMeta);
        NamespacedKey Bow = new NamespacedKey(plugin, "Teleport_BOW");
        ShapedRecipe telebow = new ShapedRecipe(Bow , itemStack);
        telebow.shape("NNN" , "NBN" , "NNN");
        telebow.setIngredient('N' , Material.NETHERITE_INGOT);
        telebow.setIngredient('B' , Material.BOW);
        itemStack.addUnsafeEnchantment(Enchantment.ARROW_INFINITE , 2 );
        if(plugin.getConfig().getBoolean("TeleportBow") && Bukkit.getServer().getRecipe(Bow) == null){
            plugin.getServer().addRecipe(telebow);
        }


    }
}
