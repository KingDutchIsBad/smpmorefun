package me.gloomified.user.stuff;

import me.gloomified.user.smpessentials.SmpEssentials;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;

public class TridentRecipe {
    public void tridentRecipe(){
        ItemStack itemStack= new ItemStack(Material.TRIDENT);
        SmpEssentials plugin = SmpEssentials.getPlugin(SmpEssentials.class);
        NamespacedKey namespacedKey = new NamespacedKey(plugin, "Trident_Recipe" );
        ShapedRecipe shapedRecipe = new ShapedRecipe(namespacedKey , itemStack);
        shapedRecipe.shape("III" , "GSG" , " D ");
        shapedRecipe.setIngredient('I' , Material.IRON_INGOT);
        shapedRecipe.setIngredient('G' , Material.GOLD_INGOT);
        shapedRecipe.setIngredient('S' , Material.STICK);
        shapedRecipe.setIngredient('D' , Material.DIAMOND);
        if(plugin.getConfig().getBoolean("Trident.enable") && Bukkit.getServer().getRecipe(namespacedKey) == null){
            plugin.getServer().addRecipe(shapedRecipe);
        }
    }
}
