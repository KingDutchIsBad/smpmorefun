package me.gloomified.user.stuff;

import me.gloomified.user.smpessentials.SmpEssentials;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class ShowHealthStick {
    SmpEssentials plugin = SmpEssentials.getPlugin(SmpEssentials.class);
    public void healthStick(){
        ItemStack itemStack = new ItemStack(Material.STICK);
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(ChatColor.GOLD+ "Show Health Stick");
        ArrayList<String> lore= new ArrayList<>();
        lore.add(ChatColor.YELLOW + "--------------------------------------");
        lore.add(ChatColor.GOLD+ "This stick shows the enemy's health");
        lore.add(ChatColor.GOLD+"after you hit them");
        lore.add(ChatColor.YELLOW + "--------------------------------------");
        itemMeta.setLore(lore);
        itemStack.setItemMeta(itemMeta);
        NamespacedKey namespacedKey = new NamespacedKey(plugin, "Health_Key" );
        ShapedRecipe shapedRecipe = new ShapedRecipe(namespacedKey , itemStack);
        shapedRecipe.shape("SSS" , "SSS" , "SSS");
        shapedRecipe.setIngredient('S' , Material.STICK);
        if(plugin.getConfig().getBoolean("Stick.enable") && Bukkit.getServer().getRecipe(namespacedKey) == null){
            plugin.getServer().addRecipe(shapedRecipe);
        }
    }
}
