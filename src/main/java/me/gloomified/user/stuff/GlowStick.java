package me.gloomified.user.stuff;

import me.gloomified.user.smpessentials.SmpEssentials;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class GlowStick implements Listener {
    SmpEssentials plugin = SmpEssentials.getPlugin(SmpEssentials.class);
    public void above(){
        ItemStack GlowStick = new ItemStack(Material.STICK);
        ItemMeta itemMeta = GlowStick.getItemMeta();
        itemMeta.setDisplayName(ChatColor.GOLD + "GlowStick");
        ArrayList<String> lore = new ArrayList<>();
        lore.add(ChatColor.GOLD + "Glow Effect I");
        itemMeta.setLore(lore);
        GlowStick.setItemMeta(itemMeta);
        NamespacedKey namespacedKey = new NamespacedKey(plugin,"Glow_Stick" );
        ShapedRecipe shapedRecipe = new ShapedRecipe(namespacedKey , GlowStick);
        shapedRecipe.shape("   " , "RSR" , "   ");
        shapedRecipe.setIngredient('R' , Material.BLAZE_ROD);
        shapedRecipe.setIngredient('S' , Material.STICK);
        if(plugin.getConfig().getBoolean("GlowStick") && Bukkit.getServer().getRecipe(namespacedKey) == null){
            plugin.getServer().addRecipe(shapedRecipe);
        }

    }
}
