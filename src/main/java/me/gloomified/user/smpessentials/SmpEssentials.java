package me.gloomified.user.smpessentials;

import me.gloomified.user.commands.Jobs;
import me.gloomified.user.commands.Rtp;
import me.gloomified.user.sumo.*;
import me.gloomified.user.events.*;
import me.gloomified.user.stuff.*;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.NamespacedKey;
import org.bukkit.command.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;


import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public  class SmpEssentials extends JavaPlugin implements CommandExecutor,TabCompleter {
    public static CustomEnchantments customEnchantments;
    public static  ArrayList<Player> players = new ArrayList<>();
    private static File file;
    private  static FileConfiguration customFile;
    public  static ArrayList<Player> playersInJobs = new ArrayList<Player>();
    public static  ArrayList<Player> playersThatJumped = new ArrayList<>();
    public static boolean vault = false;
    private static  Economy econ;

    @Override
    public void onEnable() {
        if(Bukkit.getServer().getPluginManager().getPlugin("Vault") != null){
            vault = true;
        }
        if(!setupEconomy()){
            System.out.println("Economy Plugin doesn't exist Jobs will not work");
        }
        getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&c[Smp&4Essen&3tials] &9Made by &3_KingCurry_ "));
        customEnchantments = new CustomEnchantments("Explosive_key");
        ArmourofGod stuff = new ArmourofGod();
        stuff.GodArmour();
        ToolsofGOD toolsofGOD = new ToolsofGOD();
        toolsofGOD.Tools();
        teleport_bow teleport_bow = new teleport_bow();
        teleport_bow.bow();
        Explosion_Pickaxe explosion_pickaxe = new Explosion_Pickaxe();
        explosion_pickaxe.above();
        GlowStick glowStick1 = new GlowStick();
        glowStick1.above();
        Backpack backpack = new Backpack();
        backpack.above();
        Bleeding_Sword bleeding_sword = new Bleeding_Sword();
        bleeding_sword.sword();
        Vanish_Hoe vanishHoe = new Vanish_Hoe();
        vanishHoe.vanishHoe();
        Telepathy_Shovel telepathy_shovel = new Telepathy_Shovel();
        telepathy_shovel.telepathyShovel();
        ShowHealthStick showHealthStick = new ShowHealthStick();
        showHealthStick.healthStick();
        TridentRecipe tridentRecipe = new TridentRecipe();
        tridentRecipe.tridentRecipe();
        CraftingTable craftingTable = new CraftingTable();
        craftingTable.craftingTable();
        Launch launch = new Launch();
        launch.recipe();
        getServer().getPluginManager().registerEvents(new EntityDam(), this);
        getServer().getPluginManager().registerEvents(new Interact(), this);
        getServer().getPluginManager().registerEvents(new Projectile(), this);
        getServer().getPluginManager().registerEvents(new Walk(), this);
        getServer().getPluginManager().registerEvents(new Join(), this);
        getServer().getPluginManager().registerEvents(new Inventory(), this);
        getServer().getPluginManager().registerEvents(new BlockPlace(), this);
        if(getConfig().getBoolean("Sumo.enable")){
            getServer().getPluginManager().registerEvents( new Events() , this);
        }
        getCommand("Backpack").setExecutor(new me.gloomified.user.commands.Backpack());
        getCommand("SmpEssentials").setExecutor(new me.gloomified.user.commands.Info());
        getCommand("vanishtest").setExecutor(new me.gloomified.user.commands.Vanish());
        getCommand("rtp").setExecutor(new Rtp());
        getCommand("sumo").setExecutor(this);
        getCommand("Jobs").setExecutor(new Jobs());
        new ArenaManager(this);
        getConfig().options().copyDefaults();
        saveDefaultConfig();
        registerEnchantment(customEnchantments);
        setup();
        save();
        ArenaManager.getManager().loadGames();

    }
    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    public static Economy getEconomy() {
        return econ;
    }

    public  void registerEnchantment(Enchantment enchantment) {
        boolean registered = true;
        try {
            Field f = Enchantment.class.getDeclaredField("acceptingNew");
            f.setAccessible(true);
            f.set(null, true);
            Enchantment.registerEnchantment(enchantment);
        } catch (Exception e) {
            registered = false;
            getLogger().severe(e.getStackTrace().toString());
        }
        if (registered) {
            System.out.println("Enchantment Registered");
        }
    }
  
    
    @Override
    public void onDisable() {
        try {
            Field keyField = Enchantment.class.getDeclaredField("byKey");
            keyField.setAccessible(true);
            @SuppressWarnings("unchecked")
            HashMap<NamespacedKey, Enchantment> byKey = (HashMap<NamespacedKey, Enchantment>) keyField.get(null);
            byKey.remove(customEnchantments.getKey());
            Field nameField = Enchantment.class.getDeclaredField("byName");
            nameField.setAccessible(true);
            @SuppressWarnings("unchecked")
            HashMap<String, Enchantment> byName = (HashMap<String, Enchantment>) nameField.get(null);
            byName.remove(customEnchantments.getName());
        } catch (Exception e) {
            // deepcode ignore dontUsePrintStackTrace: MEH let it print the stack trace
            e.printStackTrace();
        }
    }
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender instanceof Player)){
            ChatMsgUtil.sendMessage(sender, "&4You have to be a player to use this command");
        }
        if(args.length == 0){
            ChatMsgUtil.sendMessage(sender , "&4Sorry but there has to be a argument after /sumo ");
        }
        if(sender instanceof Player){
                if(command.getName().equalsIgnoreCase("sumo")){
                    Player player = (Player) sender;
                    if(player.hasPermission("SmpEssentials.sumo.admin")){
                        if(args.length == 1 ){
                            if(args[0].equalsIgnoreCase( "add")){
                                ArenaManager.getManager().createArena(player.getLocation());
                                ChatMsgUtil.sendMessage(player ,"&6[SmpEssentials] &3Arena added at " + player.getLocation().toString());
                            }
                        }
                    }
                    if(args.length == 2){
                        if(args[0].equalsIgnoreCase("join")){
                            if(player.hasPermission("SmpEssentials.sumo.join")){
                                int number = Integer.parseInt(args[1]);
                                if(ArenaManager.getManager().getArena(number) != null){
                                    if(!ArenaManager.getManager().isInGame(player)) {
                                        if(ArenaManager.getManager().getArena(number).getPlayers().size() < 2){
                                            ArenaManager.getManager().addPlayer(player , number);
                                            player.setGameMode(GameMode.ADVENTURE);
                                        }else {
                                            player.sendMessage("Already full");
                                        }
                                    }else {
                                        player.sendMessage("You are already in a game");
                                    }
                                }else{
                                    player.sendMessage("That Arena Doesn't exist");
                                }
                            }
                        }
                    }
                    if(args.length == 2) {
                       if (sender.hasPermission("SmpEssentials.sumo.admin")) {
                           if (args[0].equalsIgnoreCase("remove")) {
                               if (args[1] == null) return false;
                               int number = Integer.parseInt(args[1]);
                               if (get().getIntegerList("Arenas.Arenas").contains(number)) {
                                   ArenaManager.getManager().removeArena(number);
                               } else {
                                   ChatMsgUtil.sendMessage(player, "&4That arena doesn't exist");
                               }
                           }
                       }
                   }
                   if(args.length == 1){
                    if(args[0].equalsIgnoreCase("leave")){
                        if(ArenaManager.getManager().isInGame(player)){
                            ArenaManager.getManager().removePlayer(player);
                            ChatMsgUtil.sendMessage(player, "&6You have successfully left");
                        }else{
                            ChatMsgUtil.sendMessage(player , "&6You're not in a game.");
                        }

                    }
                   }
            }
        }


        return false;
    }
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        List<String> completionsArgs1 = new ArrayList<>();
        if(sender.hasPermission("SmpEssentials.sumo.admin")){ completionsArgs1.add("add"); completionsArgs1.add("remove"); }
        completionsArgs1.add("leave");
        completionsArgs1.add("join");
        if(command.getName().equalsIgnoreCase("Sumo")){
            if(args.length == 1){
                return completionsArgs1;
            }
        }
        return null;
    }


    public static   void setup(){
        file = new File(Bukkit.getServer().getPluginManager().getPlugin("SmpEssentials").getDataFolder(), "data.yml");

        if (!file.exists()){
            try{
                file.createNewFile();
            }catch (IOException e){
                //owww
            }
        }
        customFile = YamlConfiguration.loadConfiguration(file);
    }
    public  static FileConfiguration get(){
        return customFile;
    }

    public static void save(){
        try{
            customFile.save(file);
        }catch (IOException e){
            System.out.println("Couldn't save file");
        }
    }
    public static   void reload(){
        customFile = YamlConfiguration.loadConfiguration(file);
    }
}

