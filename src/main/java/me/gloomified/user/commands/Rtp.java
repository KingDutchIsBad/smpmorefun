package me.gloomified.user.commands;

import me.gloomified.user.smpessentials.TeleportUtils;
import me.gloomified.user.sumo.ChatMsgUtil;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Rtp implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player){
            if(sender.hasPermission("SmpEssentials.use.teleport")){
                Player player = (Player) sender;
                player.teleport(TeleportUtils.findSafeLocation(player));
                player.playSound(player.getLocation(),
                        Sound.ENTITY_ENDER_PEARL_THROW , 1, 0.1f);
                ChatMsgUtil.sendMessage(player,
                        "&3Wooosh");
            }
        }
        return true;
    }
}
