package me.gloomified.user.commands;

import me.gloomified.user.smpessentials.SmpEssentials;
import me.gloomified.user.sumo.ChatMsgUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Jobs implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player){
            if(args.length == 1){
                if(sender.hasPermission("SmpEssentials.jobs.use")){
                    if(args[0].equalsIgnoreCase("join")){
                        if(!SmpEssentials.playersInJobs.contains(sender)){
                            SmpEssentials.playersInJobs.add((Player) sender);
                            ChatMsgUtil.sendMessage(sender , "&3You have successfully joined jobs");
                            ChatMsgUtil.sendMessage(sender , "&3After leaving you're jobs will be reset.");
                        }
                    }
                    if(args[0].equalsIgnoreCase("leave")){
                        SmpEssentials.playersInJobs.remove(sender);
                        ChatMsgUtil.sendMessage(sender , "&3You have successfully left jobs");
                    }
                }
            }
        }
        return true;
    }
}
